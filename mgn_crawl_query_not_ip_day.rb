require 'net/http'
require 'uri'
require 'mechanize'
require 'json'
require 'active_record'

Encoding.default_external = "UTF-8"

ActiveRecord::Base.establish_connection({
                                          :adapter => "mysql",
                                          :encoding => "utf8",
                                          :host => "localhost",
                                          :username => "metagana",
                                          :password => "metagana",
                                          :database => "metagana_www"
                                        })


class MgnToken < ActiveRecord::Base
  self.table_name = 'mgn_tokens'
end

class MgnSite < ActiveRecord::Base
  self.table_name = 'mgn_sites'
end

class MgnSiteGoal < ActiveRecord::Base
  self.table_name = 'mgn_site_goals'
end

class MgnQueryNotIpResult < ActiveRecord::Base
  self.table_name = 'mgn_query_not_ip_results'
end

class MgnQueryNotIpGoalCompletion < ActiveRecord::Base
  self.table_name = 'mgn_query_not_ip_goal_completions'
end

class MgnQueryNotIpMetric < ActiveRecord::Base
  self.table_name = 'mgn_query_not_ip_metrics'
end

class MgnSpeedLoadPage < ActiveRecord::Base
  self.table_name = 'mgn_speed_load_pages'
end

class QueryCrawler
  def initialize
    @agent = Mechanize.new do |agent|
      agent.follow_meta_refresh = true
      agent.redirect_ok = true
      agent.user_agent_alias = 'Linux Mozilla'
    end
  end
  
  def get_access_token( str_client_id, str_client_secret, str_refresh_token )
    begin
      url = "https://accounts.google.com/o/oauth2/token"
      uri = URI.parse( url )
      http = Net::HTTP.new( uri.host, uri.port )
      http.use_ssl = true
      http.verify_mode = OpenSSL::SSL::VERIFY_NONE
      request = Net::HTTP::Post.new( url )
      request.set_form_data({
                            "client_id" => "#{str_client_id}",
                            "client_secret" => "#{str_client_secret}",
                            "refresh_token" => "#{str_refresh_token}",
                            "grant_type" => "refresh_token",
                            "Content-Type" => "application/x-ww-form-urlencoded"
                          })
      request.add_field("Accept", "application/json")
      response = http.request( request )
      #puts response.code
      json = response.read_body
      hash_json = JSON.parse( json )
      access_token = hash_json['access_token']
      return access_token
    rescue Exception => msg
      puts msg
    end
  end
  
  def crawl_query_data( access_token, site_id, ids, query_date )
    puts "query results"
    begin
      #query_date_start = "2014-04-02"
      #query_date_end = "2014-04-02"
      queries = [
                 "dimensions=ga:sessionDurationBucket,ga:browser,ga:keyword,ga:operatingSystemVersion,ga:city,ga:sourceMedium,ga:date&metrics=ga:users,ga:sessions,ga:newUsers,ga:bounces,ga:pageviews,ga:sessionDuration",
                 "dimensions=ga:sessionDurationBucket,ga:browser,ga:keyword,ga:operatingSystemVersion,ga:city,ga:sourceMedium,ga:deviceCategory&metrics=ga:users",
                 "dimensions=ga:sessionDurationBucket,ga:browser,ga:keyword,ga:operatingSystemVersion,ga:city,ga:sourceMedium,ga:country&metrics=ga:users",
                 "dimensions=ga:sessionDurationBucket,ga:browser,ga:keyword,ga:operatingSystemVersion,ga:city,ga:sourceMedium,ga:source&metrics=ga:users",
                 "dimensions=ga:sessionDurationBucket,ga:browser,ga:keyword,ga:operatingSystemVersion,ga:city,ga:sourceMedium,ga:operatingSystem&metrics=ga:users"
    ]
      query_results = Array.new
      queries.each do |q|
        url = "https://www.googleapis.com/analytics/v3/data/ga?ids=#{ids}&#{q}&start-date=#{query_date}&end-date=#{query_date}&access_token=#{access_token}"
        page = @agent.get( url )
        str_query = page.body
        hash_query_json = JSON.parse( str_query )
        arr_queries = hash_query_json['rows']
        query_results.push( arr_queries )
        sleep( 1 )
      end
      # ** check arr_queries Nil? - If arr_queries == true then create record null **
      unless query_results[0].nil? then
        arr_lengths = Array.new
        ( 0...5 ).each{ |i|
          arr_lengths.push( query_results[i].length )
        }
        n = arr_lengths.min
        ( 0...n ).each do |i|
          query_results[0][i].push(query_results[1][i][6], query_results[2][i][6], query_results[3][i][6], query_results[4][i][6] )
        end
        arr_visitlengths = Array.new
        query_results[0].each do |record|
          arr_visitlengths.push( record[0] )
          qs_c = MgnQueryNotIpResult.where( :site_id => site_id,:visit_length => record[0],:browser => record[1], :operating_system_version => record[3], :keyword => record[2], :city => record[4], :source_medium => record[5], :visitor_date => record[6] )
          if qs_c.empty? then
            mqr = MgnQueryNotIpResult.create( :site_id => site_id, :visit_length => record[0], :browser => record[1] , :operating_system => record[16], :operating_system_version => record[3], :device_category => record[13], :country => record[14], :city => record[4], :source => record[15], :source_medium => record[5], :keyword => record[2], :visitor_date => record[6], :visitors => record[7], :visits => record[8], :new_visits => record[9], :bounces => record[10], :pageviews => record[11], :time_on_site => record[12], :query_time => Time.now )
            mqr.save      
          end
        end
        q_v = MgnQueryNotIpResult.where( :site_id => site_id, :visitor_date => query_date )
        q_v.each do |qi|
          if !arr_visitlengths.include?( qi.visit_length ) then
            qi.destroy
          end
        end   
      end   
      return true
    rescue Exception => msg
      puts msg
      return false
    end
  end
  
  def crawl_query_metric( access_token, site_id, ids, query_date )
    begin
      url = "https://www.googleapis.com/analytics/v3/data/ga?ids=#{ids}&dimensions=ga:date&metrics=ga:users,ga:sessions,ga:newUsers,ga:bounces,ga:pageviews,ga:sessionDuration&start-date=#{query_date}&end-date=#{query_date}&access_token=#{access_token}"
      page = @agent.get( url )
      str_query = page.body
      hash_query_json = JSON.parse( str_query )
      arr_queries = hash_query_json['rows']
      unless arr_queries.nil? then
        arr_queries.each do |record|
          mqm_c = MgnQueryNotIpMetric.where( :site_id => site_id, :visitor_date => record[0] )
          if mqm_c.empty? then
            mqm = MgnQueryNotIpMetric.create( :site_id => site_id, :visitors => record[1], :visits => record[2], :new_visits => record[3], :bounces => record[4], :pageviews => record[5], :time_on_site => record[6], :visitor_date => record[0], :query_time => Time.now )
            mqm.save
          else
            mqm_c.each do |qi|
              qi.visits = record[2]
              qi.visitors = record[1]
              qi.new_visits = record[3]
              qi.bounces = record[4]
              qi.pageviews = record[5]
              qi.time_on_site = record[6]
              qi.save
            end
          end
        end
      end
    rescue Exception => msg
      puts msg
    end
  end

  def crawl_query_speed_load_page( access_token, site_id, ids, query_date )
    puts "query speed"
#    puts query_date
    begin
     # query_date_start = "2014-04-02"
      #query_date_end = "2014-04-02"
      query = "dimensions=ga:visitLength,ga:date,ga:city,ga:source,ga:operatingSystemVersion,ga:networkLocation,ga:hour&metrics=ga:visits,ga:pageLoadTime"
      url = "https://www.googleapis.com/analytics/v3/data/ga?ids=#{ids}&#{query}&start-date=#{query_date}&end-date=#{query_date}&access_token=#{access_token}"
      page = @agent.get( url )
      str_query = page.body
      hash_query_json = JSON.parse( str_query )
      arr_queries = hash_query_json['rows']
      unless arr_queries.nil? then
        arr_queries.each do |record|
          qspl_c = MgnSpeedLoadPage.where( :site_id => site_id, :visit_length => record[0], :city => record[2], :source => record[3], :operatingSystemVersion => record[4], :networkLocation => record[5], :hour => record[6], :visit_date => query_date )
          if qspl_c.empty? then
            puts "visit #{record[1]}"
            mslp = MgnSpeedLoadPage.create( :site_id => site_id, :visit_length => record[0] , :city => record[2], :source => record[3], :operatingSystemVersion => record[4], :networkLocation => record[5], :hour => record[6], :page_load_time => record[8], :visits => record[7], :visit_date => record[1], :query_time => Time.now )
            mslp.save
          else
            qspl_c.each do |qu|
              qu.visits = record[7]
              qu.page_load_time = record[8]
              qu.save
            end
          end
        end
      end
      return true
    rescue Exception => msg
      puts msg
    end
  end
  
  def crawl_query_goal_completion( access_token, site_id, ids, query_date )
      #query_date_start = "2014-04-02"
      #query_date_end = "2014-04-02"
    puts "goal"
    begin
      qgs = MgnSiteGoal.where( :site_id => site_id )
      unless qgs.empty? then
        count = qgs.length
        str_goals = String.new
        qgs.each do |qg|
          order = qg.order
          goal_result = ",ga:goal#{order}Completions"
          str_goals = str_goals << goal_result
        end
#        puts str_goals
        url = "https://www.googleapis.com/analytics/v3/data/ga?ids=#{ids}&dimensions=ga:sessionDurationBucket,ga:date&metrics=ga:sessions#{str_goals}&start-date=#{query_date}&end-date=#{query_date}&access_token=#{access_token}"
        page = @agent.get( url )
        str_query = page.body
        hash_query_json = JSON.parse( str_query )
#        puts hash_query_json
        arr_queries = hash_query_json['rows']
        unless arr_queries.nil? then
          #mqgc = MgnQueryGoalCompletion.create( :site_id => site_id, :query_time => Time.now )
        #else
          #puts arr_queries
          arr_queries.each do |record|
            h_goals = {}
            #puts record
            arr_goals = record[3,count]
            j = 0
            qgs.each do |qg|
              name = qg.name.force_encoding('UTF-8')
              h_goals[name] = arr_goals[j]
              j += 1
            end
            goal_json = h_goals.to_json
            mqgc_c = MgnQueryNotIpGoalCompletion.where( :site_id => site_id, :visit_length => record[0], :goal_completions => goal_json, :date => query_date )
            if mqgc_c.empty? then
              #puts goal_json
              mqgc = MgnQueryNotIpGoalCompletion.create( :site_id => site_id, :visit_length => record[0] ,:date => record[1], :visits => record[2], :goal_completions => goal_json, :query_time => Time.now )
              mqgc.save
            else
              mqgc_c.each do |gi|
                gi.visits = record[2]
                gi.save
              end
            end
          end
        end
      end
    rescue Exception => e
      puts e
    end
  end
end

def crawl_query_main
  #fp = File.open( "/home/tung/metagana/date2.txt")
  #while line = fp.gets do
  mt_first = MgnToken.first
  qc = QueryCrawler.new
  client_id = mt_first.client_id
  client_secret = mt_first.client_secret
  refresh_token = mt_first.refresh_token
  access_token = qc.get_access_token( client_id, client_secret, refresh_token )
  qs = MgnSite.where( :stt => 1 )
  query_date = Date.today.prev_day.to_s
   # query_date = line.gsub("\n", "")
  #    puts line
  qs.each do |j|
    site_id = j.id
    puts site_id
    ids = j.client_ids
      #puts ids
    #      qc.crawl_query_speed_load_page( access_token, site_id, ids, query_date )
    qc.crawl_query_metric( access_token, site_id, ids, query_date )
    qc.crawl_query_goal_completion( access_token, site_id, ids, query_date )
    qc.crawl_query_data( access_token, site_id, ids, query_date )
  end

  #end
end

crawl_query_main()
