require 'net/https'
require 'uri'
require 'json'
require 'mechanize'
require 'active_record'

Encoding.default_external = "UTF-8"

ActiveRecord::Base.establish_connection({
                                          :adapter => "mysql",
                                          :encoding => "utf8",
                                          :host => "localhost",
                                          :username => "metagana",
                                          :password => "metagana",
                                          :database => "metagana_www"
                                        })

class MgnToken < ActiveRecord::Base
  self.table_name = 'mgn_tokens'
end

class MgnCustomerGa < ActiveRecord::Base
  self.table_name = 'mgn_customer_gas'
end

class MgnSite < ActiveRecord::Base
  self.table_name = 'mgn_sites'
end

class MgnSiteGoal < ActiveRecord::Base
  self.table_name = 'mgn_site_goals'
end

class QueryCrawler

  def initialize
  end
  
  def get_access_token( str_client_id, str_client_secret, str_refresh_token )
    begin
      url = "https://accounts.google.com/o/oauth2/token"
      uri = URI.parse( url )
      http = Net::HTTP.new( uri.host, uri.port )
      http.use_ssl = true
      http.verify_mode = OpenSSL::SSL::VERIFY_NONE
      request = Net::HTTP::Post.new( url )
      request.set_form_data({
                              "client_id" => "#{str_client_id}",
                              "client_secret" => "#{str_client_secret}",
                              "refresh_token" => "#{str_refresh_token}",
                              "grant_type" => "refresh_token",
                              "Content-Type" => "application/x-ww-form-urlencoded"
                            })
      request.add_field("Accept", "application/json")
      response = http.request( request )
      json = response.read_body
      hash_json = JSON.parse( json )
      access_token = hash_json['access_token']
      return access_token
    rescue Exception => e
      puts e
    end
  end
  
  def crawl_data( url, access_token )
    begin
      #puts url
      encoded_url = URI.encode( url )
      #puts encoded_url
      uri = URI.parse( encoded_url )
      puts uri
      http = Net::HTTP.new(uri.host, uri.port)
      http.use_ssl = true
      request = Net::HTTP::Get.new(uri.request_uri, {"Authorization" => "Bearer #{access_token}", "X-JavaScript-User-Agent" => "Google APIs Explorer"})
      response = http.request(request)
      str_query = response.body
      hash_query_json = JSON.parse( str_query )
      return hash_query_json
    rescue Exception => e
     puts e
    end
  end
  
  def get_customer_ga( access_token )
    begin
      url = 'https://www.googleapis.com/analytics/v3/management/accounts'    
      hash_query_json = crawl_data( url, access_token )
      if hash_query_json["totalResults"].to_i > 0 then
        arr_queries = hash_query_json['items']
        puts arr_queries
        arr_ids = Array.new
        arr_queries.each do |query|
          arr_ids.push( query["id"] )
        end
        qc = MgnCustomerGa.all        
        qc.each do |qi|
          if !arr_ids.include?( qi.ga_id ) then
            qs = MgnSite.where( :customer_ga_id => qi.id )
            if !qs.empty? then
              qs.each do |q_|
                q_.stt = 0
                q_.save
              end
            end
            qi.destroy
          end
        end
        
        if qc.empty? then
          arr_queries.each do |query|
            child_link = query["childLink"]
            mc = MgnCustomerGa.create( :ga_id => query["id"], :ga_name => query["name"], :ga_url => child_link["href"] )
            mc.save
          end
        else
          arr_queries.each do |query|
            child_link = query["childLink"]
            q = MgnCustomerGa.where( :ga_id => query["id"] )
            if q.empty? then
              mc = MgnCustomerGa.create( :ga_id => query["id"], :ga_name => query["name"], :ga_url => child_link["href"] )
              mc.save
            else
              q.each do |i|
                if i.ga_name != query["name"] then
                  i.ga_name = query["name"]
                  i.save
                end
              end
            end
          end
        end
      else
        qc = MgnCustomerGa.all
        if !qc.empty? then
          qc.each do |qi|
            qs = MgnSite.where( :customer_ga_id => qi.id )
            if !qs.empty? then
              qs.each do |q_|
                q_.stt = 0
                q_.save
              end
            end
            qi.destroy
          end
        end
      end
    rescue Exception => e
      puts e
    end
  end
  
  def get_info_properties( access_token )
    begin
      qc = MgnCustomerGa.all
      if !qc.empty? then
        qc.each do |q|
          begin
            hash_results = crawl_data( q.ga_url, access_token )
            arr_items = hash_results["items"]
            arr_items.each do |arr_item|
              child_link = arr_item["childLink"]
              url = child_link["href"]
              hash_query_results = crawl_data( url, access_token )
              if hash_query_results["totalResults"].to_i > 0 then
                items = hash_query_results["items"]
                #puts items
                #puts "-----------------------------------------"
                items.each do |item|
                  link_goal = item["childLink"]
                  sub_url = item["websiteUrl"].to_s.gsub( "http://", "" ).gsub( "index.html", "").gsub( "index.php", "" ).gsub( "index.htm", "" )
                  if sub_url[-1] == "/" then
                    domain = sub_url[ 0..-2 ]
                  else
                    domain = sub_url
                  end
                  arr_domain = Array.new
                  sub_domain = domain.gsub( "www.", "" )
                  arr_domain.push( domain, sub_domain )
                  arr_domain.each do |d|
                    ids = item["id"]
                    qs = MgnSite.where( :extract_domain => d, :stt => 0 )
                    if !qs.empty? then
                      qs.each do |j|
                        j.customer_ga_id = q.id
                        j.property_id = item["webPropertyId"]
                        j.client_ids = "ga:#{ids}"
                        j.child_link = link_goal["href"]
                        j.stt = 1
                        j.save
                      end
                    end
                    qs_ = MgnSite.where( :domain => d, :stt => 0 )
                    if !qs_.empty? then
                      ids = item["id"]
                      qs_.each do |l|
                        l.customer_ga_id = q.id
                        l.property_id = item["webPropertyId"]
                        l.client_ids = "ga:#{ids}"
                        l.child_link = link_goal["href"]
                        l.stt = 1
                        l.save
                      end
                    end
                    qs__ = MgnSite.where( :extract_domain => "www.#{d}", :stt => 0 )
                    if !qs__.empty? then
                      qs__.each do |j|
                        j.customer_ga_id = q.id
                        j.property_id = item["webPropertyId"]
                        j.client_ids = "ga:#{ids}"
                        j.child_link = link_goal["href"]
                        j.stt = 1
                        j.save
                      end
                    end
                    qs__i = MgnSite.where( :domain => "www.#{d}", :stt => 0 )
                    if !qs__i.empty? then
                      ids = item["id"]
                      qs__i.each do |l|
                        l.customer_ga_id = q.id
                        l.property_id = item["webPropertyId"]
                        l.client_ids = "ga:#{ids}"
                        l.child_link = link_goal["href"]
                        l.stt = 1
                        l.save
                      end
                    end
                  end
=begin
                  arr_domain.each do |dm|
                    qs_ = MgnSite.where( :domain => dm, :stt => 0 )
                    if !qs_.empty? then
                      ids = item["id"]
                      qs_.each do |l|
                        l.customer_ga_id = q.id
                        l.property_id = item["webPropertyId"]
                        l.client_ids = "ga:#{ids}"
                        l.child_link = link_goal["href"]
                        l.stt = 1
                        l.save
                      end
                    end
                  end
=end
                end
              end
            end  
          rescue
          end
        end
      end
    rescue 
    end
  end
  
  def get_conversion_goals( access_token )
    qs_g = MgnSite.where( :stt => 1 )
    qs_g.each do |qs|
      begin
        hash_results = crawl_data( qs.child_link, access_token )
        if hash_results["totalResults"].to_i > 0 then
          arr_hash_items = hash_results["items"]
          qsg_id = MgnSiteGoal.where( :site_id => qs.id )
          if qsg_id.empty? then
            arr_hash_items.each do |hash_items|
              hash_url = hash_items["urlDestinationDetails"]
              msg_c = MgnSiteGoal.create( :site_id => qs.id, :name => hash_items["name"], :url => hash_url["url"], :active => hash_items["active"], :order => hash_items["id"])
              msg_c.save
            end
          else
            arr_hash_items.each do |hash_items|
              hash_url = hash_items["urlDestinationDetails"]
              msg_check = MgnSiteGoal.where( :site_id => qs.id, :order => hash_items["id"] )
              if msg_check.empty? then
                msg_c = MgnSiteGoal.create( :site_id => qs.id, :name => hash_items["name"], :url => hash_url["url"], :active => hash_items["active"], :order => hash_items["id"] )
                msg_c.save
              else
                msg_check_l2 = MgnSiteGoal.where( :site_id => qs.id, :name => hash_items["name"], :url => hash_url["url"], :active => hash_items["active"], :order => hash_items["id"] )
                if msg_check_l2.empty? then
                  qmsg = MgnSiteGoal.where( :site_id => qs.id, :order => hash_items["id"] )
                  qmsg.each do |qms_|
                    qms_.name = hash_items["name"]
                    qms_.url = hash_url["url"]
                    qms_.active = hash_items["active"]
                    qms_.save
                  end
                end
              end
            end
          end
        end
      rescue Exception => e
        puts e
      end
    end
  end
  
end

def crawl_query_main
  mt_first = MgnToken.first
  qc = QueryCrawler.new
  client_id = mt_first.client_id
  client_secret = mt_first.client_secret
  refresh_token = mt_first.refresh_token
  access_token = qc.get_access_token( client_id, client_secret, refresh_token )
  qc.get_customer_ga( access_token )
  qc.get_info_properties( access_token )
  qc.get_conversion_goals( access_token )
end

crawl_query_main()
