require 'net/http'
require 'uri'
require 'mechanize'
require 'json'
require 'active_record'

Encoding.default_external = "UTF-8"

ActiveRecord::Base.establish_connection({
                                          :adapter => "mysql",
                                          :encoding => "utf8",
                                          :host => "localhost",
                                          :username =>"metagana",
                                          :password => "metagana",
                                          :database => "metagana_www"
                                        })
class MgnToken < ActiveRecord::Base
  self.table_name = 'mgn_tokens'
end

class MgnSite < ActiveRecord::Base
  self.table_name = 'mgn_sites'
end

class MgnQueryRealtimeResult < ActiveRecord::Base
  self.table_name = 'mgn_query_realtime_results'
end

class QueryCrawler
  def initialize
    @agent = Mechanize.new do |agent|
      agent.follow_meta_refresh = true
      agent.redirect_ok = true
      agent.user_agent_alias = 'Linux Mozilla'
    end
  end
 # **Get access token ** 
  def get_access_token( str_client_id, str_client_secret, str_refresh_token )
    begin
      url = "https://accounts.google.com/o/oauth2/token"
      uri = URI.parse( url )
      http = Net::HTTP.new( uri.host, uri.port )
      http.use_ssl = true
      http.verify_mode = OpenSSL::SSL::VERIFY_NONE
      request = Net::HTTP::Post.new( url )
      request.set_form_data({
                              "client_id" => "#{str_client_id}",
                              "client_secret" => "#{str_client_secret}",
                              "refresh_token" => "#{str_refresh_token}",
                              "grant_type" => "refresh_token",
                              "Content-Type" => "application/x-ww-form-urlencoded"
                            })
      request.add_field("Accept", "application/json")
      response = http.request( request )
      #puts response.code
      json = response.read_body
      hash_json = JSON.parse( json )
      access_token = hash_json['access_token']
      return access_token
    rescue Exception => msg
      puts msg
    end
  end
  
  def crawl_query_data( access_token, site_id, ids )
    begin
      #puts site_id
      #puts ids
      query = "dimensions=ga:pagePath&metrics=ga:activeVisitors"
      url = "https://www.googleapis.com/analytics/v3/data/realtime?ids=#{ids}&#{query}&access_token=#{access_token}"
      #exit
      page = @agent.get( url )
      str_query = page.body
      puts str_query
      hash_query_json = JSON.parse( str_query )
      hash_query_active_visitors = hash_query_json['totalsForAllResults']
      active_visitors = hash_query_active_visitors["ga:activeVisitors"]
      arr_queries = hash_query_json['rows']
      time_query = Time.now()
      mqrr_check = MgnQueryRealtimeResult.where( :site_id => site_id )
      if !arr_queries.nil? then
        arr_pages = Array.new
        hash_pages = {}
        arr_queries.each do |query|
          hash_pages[query[0]] = query[1]
        end
        # **sort and get top page**
        sort_page_hash = hash_pages.sort_by { |_key, value| value }
        n = sort_page_hash.length
        arr_page_tops = Array.new
        k = 0
        sort_page_hash.each do |arr|
          k += 1
          arr_page_tops.push( sort_page_hash[n - k].join( ":" ) )
          if k > 10 then
            break
          end
        end
        json_page_tops = arr_page_tops.to_json
        if mqrr_check.empty? then
          stt = 25
          while stt >= 5 do
            mq = MgnQueryRealtimeResult.create( :site_id => site_id, :active_visitors => 0, :top_page_viewed => "", :status => stt, :query_time => Time.now )
            mq.save
            stt -= 5
          end
          mq = MgnQueryRealtimeResult.create( :site_id => site_id, :active_visitors => active_visitors, :top_page_viewed => json_page_tops, :status => 0, :query_time => Time.now)
          mq.save
        else
          stt = 20
          while stt >= 0 do
            MgnQueryRealtimeResult.where( :site_id => site_id, :status => stt ).each do |mqi|
              stt_u = stt + 5
              MgnQueryRealtimeResult.where( :site_id => site_id, :status => stt_u ).each do |mqj|
                mqj.active_visitors = mqi.active_visitors
                mqj.top_page_viewed = mqi.top_page_viewed
                mqj.update_time = Time.now
                mqj.save
              end
            end
            stt -= 5
          end
          #puts active_visitors
          #puts json_page_tops
          MgnQueryRealtimeResult.where( :site_id => site_id, :status => 0 ).each do |mq|
            mq.active_visitors = active_visitors
            mq.top_page_viewed = json_page_tops
            mq.update_time = Time.now
            mq.save
          end
        end
      else
        if mqrr_check.empty? then
          stt = 25
          while stt >= 0 do
            mq = MgnQueryRealtimeResult.create(  :site_id => site_id, :active_visitors => 0, :top_page_viewed => "", :status => stt, :query_time => Time.now )
            mq.save
            stt -= 5
          end
        else
          stt = 20
          while stt >= 0 do
            MgnQueryRealtimeResult.where( :site_id => site_id, :status => stt ).each do |mqi|
              stt_u = stt + 5
              MgnQueryRealtimeResult.where( :site_id => site_id, :status => stt_u ).each do |mqj|
                mqj.active_visitors = mqi.active_visitors
                mqj.top_page_viewed = mqi.top_page_viewed
                mqj.update_time = Time.now
                mqj.save
              end
            end
            stt -= 5
          end
          MgnQueryRealtimeResult.where( :site_id => site_id, :status => 0 ).each do |mq|
            mq.active_visitors = 0
            mq.top_page_viewed = ""
            mq.update_time = Time.now
            mq.save
          end
        end
      end
    rescue Exception => msg
      puts msg
    end
  end
end

def crawl_query_main
  mt_first = MgnToken.first
  qc = QueryCrawler.new 
  client_id = mt_first.client_id
  client_secret = mt_first.client_secret
  refresh_token = mt_first.refresh_token
  access_token = qc.get_access_token( client_id, client_secret, refresh_token )
  qs = MgnSite.where( :stt => 1 )
  qs.each do |j|
    site_id = j.id
    ids = j.client_ids
    #puts ids
    qc.crawl_query_data( access_token, site_id, ids )
    sleep( 1 )
  end
end

crawl_query_main()
