require 'active_record'
ActiveRecord::Base.establish_connection({
                                          :adapter => "mysql",
                                          :encoding => "utf8",
                                          :host => "localhost",
                                          :username => "metagana",
                                          :password => "metagana",
                                          :database => "metagana_www"
                                        })


class MgnSite < ActiveRecord::Base
  self.table_name = 'mgn_sites'
end

q = MgnSite.all
q.each do |q_|
  source = q_.domain
  sub_url = source.gsub( "http://", "" ).gsub( "index.html", "").gsub( "index.php", "" ).gsub( "index.htm", "" )
  if sub_url[-1] == "/" then
    ex_domain = sub_url[ 0..-2 ]
  else
    ex_domain = sub_url
  end
  q_.extract_domain = ex_domain
  q_.save
end
