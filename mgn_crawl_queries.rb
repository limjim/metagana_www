require 'net/http'
require 'uri'
require 'mechanize'
require 'json'
require 'active_record'

Encoding.default_external = "UTF-8"

ActiveRecord::Base.establish_connection({
                                          :adapter => "mysql",
                                          :encoding => "utf8",
                                          :host => "localhost",
                                          :username => "metagana",
                                          :password => "metagana",
                                          :database => "metagana_www"
                                        })


class MgnToken < ActiveRecord::Base
  self.table_name = 'mgn_tokens'
end

class MgnSite < ActiveRecord::Base
  self.table_name = 'mgn_sites'
end

class MgnQueryResult < ActiveRecord::Base
  self.table_name = 'mgn_query_results'
end

class MgnQueryPage < ActiveRecord::Base
  self.table_name = 'mgn_query_pages'
end

class MgnSpeedLoadPage < ActiveRecord::Base
  self.table_name = 'mgn_speed_load_pages'
end

class MgnSiteGoal < ActiveRecord::Base
  self.table_name = 'mgn_site_goals'
end

class MgnQueryGoalCompletion < ActiveRecord::Base
  self.table_name = 'mgn_query_goal_completions'
end

class QueryCrawler
  def initialize
    @agent = Mechanize.new do |agent|
      agent.follow_meta_refresh = true
      agent.redirect_ok = true
      agent.user_agent_alias = 'Linux Mozilla'
    end
  end
  
  def get_access_token( str_client_id, str_client_secret, str_refresh_token )
    begin
      url = "https://accounts.google.com/o/oauth2/token"
      uri = URI.parse( url )
      http = Net::HTTP.new( uri.host, uri.port )
      http.use_ssl = true
      http.verify_mode = OpenSSL::SSL::VERIFY_NONE
      request = Net::HTTP::Post.new( url )
      request.set_form_data({
                            "client_id" => "#{str_client_id}",
                            "client_secret" => "#{str_client_secret}",
                            "refresh_token" => "#{str_refresh_token}",
                            "grant_type" => "refresh_token",
                            "Content-Type" => "application/x-ww-form-urlencoded"
                          })
      request.add_field("Accept", "application/json")
      response = http.request( request )
      #puts response.code
      json = response.read_body
      hash_json = JSON.parse( json )
      access_token = hash_json['access_token']
      return access_token
    rescue Exception => msg
      puts msg
    end
  end
  
  def group_page_index_duplicate( h )
    val = 0
    flag = false
    h.each{ |k,v|
      #puts "#{k} : #{v}"
      t = k.slice( 0..5 )
      if k == "/" || t == "/index"  then
        val += v
        if t == "/index" then
          h.delete( k )
          flag = true
        end
      end
    }
    if flag == true then
      h["/"] = val
    end
    arr = Array.new
    h.each{ |k,v|
      c = k.length - 1
      if ( c > 0 && k[c] == "/" || k =~ /index/ ) then
        arr.push( k )
      end
    }
    arr_results = Array.new
    arr.each do |i|
      str = i.gsub( /index(.*)/, "" ).slice( 0 .. -2 )
      arr_results.push( str )
    end
    arr_results = arr_results.uniq  
    arr_results.each do |j|
      val1 = 0
      flag1 = false
      str_s = j.to_s
      h.each{ |k,v|
        z = k.length - 1
        str_r = "#{str_s}/index"
        if ( z > 0 && k == j || k =~ /#{str_r}/ || k == "#{str_s}/" ) then
          val1 += v
          if ( k =~ /#{str_r}/ || k == "#{str_s}/" ) then
            h.delete( k )
            flag1 = true
          end
        end
      }
      if flag == true then
        h["#{j}"] = val1
      end
    end
    return h
  end
  
  def get_ip_num( str_ip )
    begin
      arrs = str_ip.split( "." )
      return ( arrs[0].to_i * ( 256**3 ) ) + ( arrs[1].to_i * ( 256**2 ) ) + ( arrs[2].to_i * 256 ) + arrs[3].to_i
    rescue
      return false
    end
  end
  
  def crawl_query_data( access_token, site_id, ids, query_date )
    puts "query results"
    begin
      #query_date_start = "2014-04-02"
      #query_date_end = "2014-04-02"
      queries = [
                 "dimensions=ga:sessionDurationBucket,ga:dimension1,ga:exitPagePath,ga:browser,ga:operatingSystem,ga:source,ga:keyword&metrics=ga:users,ga:sessions,ga:newUsers,ga:bounces,ga:pageviews,ga:sessionDuration,ga:timeOnPage",
                 "dimensions=ga:sessionDurationBucket,ga:dimension1,ga:exitPagePath,ga:browser,ga:operatingSystem,ga:source,ga:operatingSystemVersion&metrics=ga:users",
                 "dimensions=ga:sessionDurationBucket,ga:dimension1,ga:exitPagePath,ga:browser,ga:operatingSystem,ga:source,ga:date&metrics=ga:users",
                 "dimensions=ga:sessionDurationBucket,ga:dimension1,ga:exitPagePath,ga:browser,ga:operatingSystem,ga:source,ga:country&metrics=ga:users",
                 "dimensions=ga:sessionDurationBucket,ga:dimension1,ga:exitPagePath,ga:browser,ga:operatingSystem,ga:source,ga:region&metrics=ga:users",
                 "dimensions=ga:sessionDurationBucket,ga:dimension1,ga:exitPagePath,ga:browser,ga:operatingSystem,ga:source,ga:deviceCategory&metrics=ga:users",
                 "dimensions=ga:sessionDurationBucket,ga:dimension1,ga:exitPagePath,ga:browser,ga:operatingSystem,ga:sourceMedium,ga:city&metrics=ga:users"
                 
    ]
      query_results = Array.new
      queries.each do |q|
        url = "https://www.googleapis.com/analytics/v3/data/ga?ids=#{ids}&#{q}&start-date=#{query_date}&end-date=#{query_date}&access_token=#{access_token}"
        page = @agent.get( url )
        str_query = page.body
        hash_query_json = JSON.parse( str_query )
        arr_queries = hash_query_json['rows']
        query_results.push( arr_queries )
        sleep( 1 )
      end
      # ** check arr_queries Nil? - If arr_queries == true then create record null **
      unless query_results[0].nil? then
        #mqr = MgnQueryResult.create( :site_id => site_id, :visitor_date => query_date, :query_time => Time.now )
        #mqr.save
      #else
        # ** merge array **
        arr_lengths = Array.new
        ( 0...7 ).each{ |i| 
          arr_lengths.push( query_results[i].length )
        }
        n = arr_lengths.min
        ( 0...n ).each do |i|
          query_results[0][i].push(query_results[1][i][6], query_results[2][i][6], query_results[3][i][6], query_results[4][i][6], query_results[5][i][6], query_results[6][i][5], query_results[6][i][6] )
        end
        # ** save database **
        #puts query_results[0]
        #exit
        query_results[0].each do |record|
          qs_c = MgnQueryResult.where( :site_id => site_id, :visit_length => record[0], :visitor_ip => record[1], :browser => record[3], :operating_system => record[4], :exit_page_path => record[2], :visitor_date => record[15] )
          if qs_c.empty? then
            ip_num = get_ip_num( record[1] )
            mqr = MgnQueryResult.create( :site_id => site_id, :visit_length => record[0], :visitor_ip => record[1], :visitor_ip_num => ip_num ,:exit_page_path => record[2], :browser => record[3] , :operating_system => record[4], :operating_system_version => record[14], :device_category => record[18], :country => record[16], :city => record[20], :region => record[17], :source => record[5], :source_medium => record[19], :keyword => record[6], :visitor_date => record[15], :users => record[7], :visits => record[8], :new_sessions => record[9], :bounces => record[10], :pageviews => record[11], :time_on_site => record[12], :time_on_exit_page => record[13], :query_time => Time.now )
            mqr.save      
          end
        end 
      end      
      return true
    rescue Exception => msg
      puts msg
      return false
    end
  end

  def crawl_query_pages( access_token, site_id, ids, query_date )
    puts "query pages"
    url_path = "https://www.googleapis.com/analytics/v3/data/ga?ids=#{ids}&dimensions=ga:sessionDurationBucket,ga:dimension1,ga:previousPagePath,ga:date&metrics=ga:users,ga:timeOnPage&start-date=#{query_date}&end-date=#{query_date}&access_token=#{access_token}"
    begin
      page_path = @agent.get( url_path )
      str_query_path = page_path.body
      hash_query_path_json = JSON.parse( str_query_path )
      arr_query_paths = hash_query_path_json['rows']
      unless arr_query_paths.nil? then
        arr_query_paths.each do |record|
          qs = MgnQueryResult.where( :site_id => site_id ,:visitor_date => record[3] )
          qs.each do |q|
            if ( q.visit_length == record[0] ) && ( q.visitor_ip == record[1] ) then
              #qp_check = MgnQueryPage.where( :query_id => q.id )
              #if qp_check.empty? then
               # qp_c = MgnQueryPage.create( :query_id => q.id, :visit_length => record[0], :visitor_ip => record[1], :visitor_date => record[3], :previous_page_path => record[2], :time_on_page => record[5], :query_time => Time.now )
                #qp_c.save  
              #end
              qs_check = MgnQueryPage.where( :query_id => q.id, :visit_length => record[0], :visitor_ip => record[1], :visitor_date => record[3], :previous_page_path => record[2], :time_on_page => record[5] )        
              if qs_check.empty? then
                qp_c = MgnQueryPage.create( :query_id => q.id, :visit_length => record[0], :visitor_ip => record[1], :visitor_date => record[3], :previous_page_path => record[2], :time_on_page => record[5], :query_time => Time.now )
                qp_c.save
              end
            end
          end
        end
      end
    rescue Exception => msg
      puts msg
    end
  end
  
  def crawl_query_speed_load_page( access_token, site_id, ids, query_date )
    puts "query speed"
    begin
     # query_date_start = "2014-04-02"
      #query_date_end = "2014-04-02"
      query = "dimensions=ga:sessionDurationBucket,ga:date,ga:city,ga:source,ga:operatingSystemVersion,ga:networkLocation,ga:hour&metrics=ga:sessions,ga:pageLoadTime"
      url = "https://www.googleapis.com/analytics/v3/data/ga?ids=#{ids}&#{query}&start-date=#{query_date}&end-date=#{query_date}&access_token=#{access_token}"
      page = @agent.get( url )
      str_query = page.body
      hash_query_json = JSON.parse( str_query )
      arr_queries = hash_query_json['rows']
      unless arr_queries.nil? then
        #mslp = MgnSpeedLoadPage.create( :site_id => site_id, :query_time => Time.now )
        #mslp.save
      #else
        arr_queries.each do |record|
          qspl_c = MgnSpeedLoadPage.where( :site_id => site_id, :visit_length => record[0], :city => record[2], :source => record[3], :operatingSystemVersion => record[4], :networkLocation => record[5], :hour => record[6], :visit_date => query_date )
          if qspl_c.empty? then
            mslp = MgnSpeedLoadPage.create( :site_id => site_id, :visit_length => record[0] , :city => record[2], :source => record[3], :operatingSystemVersion => record[4], :networkLocation => record[5], :hour => record[6], :page_load_time => record[8], :visits => record[7], :visit_date => record[1], :query_time => Time.now )
            mslp.save
          else
            qspl_c.each do |qu|
              qu.visits = record[7]
              qu.page_load_time = record[8]
              qu.save
            end
          end
        end
      end
      return true
    rescue Exception => msg
      puts msg
    end
  end
  
  def crawl_query_goal_completion( access_token, site_id, ids, query_date )
    #query_date_start = "2014-04-02"
    #query_date_end = "2014-04-02"
    puts "goal"
    begin
      qgs = MgnSiteGoal.where( :site_id => site_id )
      unless qgs.empty? then
        count = qgs.length
        str_goals = String.new
        qgs.each do |qg|
          order = qg.order
          goal_result = ",ga:goal#{order}Completions"
          str_goals = str_goals << goal_result
        end
        #puts str_goals
        url = "https://www.googleapis.com/analytics/v3/data/ga?ids=#{ids}&dimensions=ga:sessionDurationBucket,ga:dimension1,ga:date&metrics=ga:sessions#{str_goals}&start-date=#{query_date}&end-date=#{query_date}&access_token=#{access_token}"
        page = @agent.get( url )
        str_query = page.body
        hash_query_json = JSON.parse( str_query )
        arr_queries = hash_query_json['rows']
        unless arr_queries.nil? then
          #mqgc = MgnQueryGoalCompletion.create( :site_id => site_id, :query_time => Time.now )
        #else
          arr_queries.each do |record|
            h_goals = {}
            arr_goals = record[4,count]
            j = 0
            qgs.each do |qg|
              name = qg.name.force_encoding('UTF-8')
              h_goals[name] = arr_goals[j]
              j += 1
            end
            mqgc_c = MgnQueryGoalCompletion.where( :site_id => site_id, :visit_length => record[0], :visitor_ip => record[1], :date => query_date )
            if mqgc_c.empty? then
              ip_num = get_ip_num( record[1] )
              goal_json = h_goals.to_json
              mqgc = MgnQueryGoalCompletion.create( :site_id => site_id, :visit_length => record[0] ,:visitor_ip => record[1], :date => record[2], :visits => record[3], :goal_completions => goal_json, :visitor_ip_num => ip_num, :query_time => Time.now )
              mqgc.save
            end
          end
        end
      end
    rescue Exception => e
      puts e
    end
  end
end

def crawl_query_main
  mt_first = MgnToken.first
  qc = QueryCrawler.new
  client_id = mt_first.client_id
  client_secret = mt_first.client_secret
  refresh_token = mt_first.refresh_token
  access_token = qc.get_access_token( client_id, client_secret, refresh_token )
  qs = MgnSite.where( :stt => 1 )
  query_date = Date.today.to_s
  qs.each do |j|
    site_id = j.id
    puts site_id
    ids = j.client_ids
    #puts ids
    qc.crawl_query_goal_completion( access_token, site_id, ids, query_date )
    flag = qc.crawl_query_data( access_token, site_id, ids, query_date )
    if flag == true then        
      qc.crawl_query_pages( access_token, site_id, ids, query_date )
      flag_ = qc.crawl_query_speed_load_page( access_token, site_id, ids, query_date )
    else
      sleep( rand( 2 ) )
    end
  end
  
end

crawl_query_main()
